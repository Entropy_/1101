![screenie](screener.png)


 This is a small program to take input from a webcamera and do fun stuff with it
 in your CL window. To get it running, clone this distro, then get the deps,
 which are python-pygame and Pillow

>Under development is showing more than one webcam at a time, currently it works
>But you have to know your peers, also I should use a framework to pass the 
>messages back and forth. as right now your local framerate is ~okay~ and the 
nonlocal is ~~horrendous.

Cheers!
Phil



>>chmod +x 1101

then just set your terminal to fullscreen and start it with

>>./1101

Now, you'll notice there are two different windows with nothing in them!
Let's fix that by opening up another terminal in the workspace and running this

>>python camcontrol.py

You should see a light come on your camera and that's it.

Now, go back to your first screen and you'll find it's filled with wonderful
datas.

The odd name is because I have this set up on a computer that has a mouse and a
keypad attached to it and nothing else. I want to be able to run it with what I 
have.

The images it produces are garbage collected, but that's not stopping someone 
nefarious from grabbing it along the way. If you're paranoid about people seeing
what your webcamera sees, this is not for you.


If you like this, I've got plenty more glitchy green screen computer age toys 
that I've always wanted to build on. paypal.me/phrylo and send me a note!
